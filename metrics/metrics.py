# -*- coding: utf-8 -*-
"""

@author: mapo
"""
import itertools
import sys

import matplotlib
import numpy as np
import seaborn as sns
import matplotlib as mpl
import torch
from matplotlib import cm
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import roc_auc_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix

from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import ConfusionMatrixDisplay

from fastai.metrics import (APScoreBinary, APScoreMulti, RocAucMulti, RocAuc, RocAucBinary, MatthewsCorrCoef,
                            BalancedAccuracy, accuracy, accuracy_multi)

from torchmetrics import FBeta, Accuracy

#######################################################################################


CATEGORIES3 = {"cns": 0, "antineoplastic": 1, "cardio": 2}

CATEGORIES5 = {"cns": 0, "antineoplastic": 1, "cardio": 2, "gastrointestinal": 3, "antiinfective": 4}

CATEGORIES12 = {"cns": 0, "antineoplastic": 1, "cardio": 2, "gastrointestinal": 3, "antiinfective": 4,
                "reproductivecontrol": 5, "lipidregulating": 6, "hematologic": 7, "respiratorysystem": 8,
               "antiinflammatory": 9, "urological": 10, "dermatologic": 11}


########################################################################################
def get_categories(num_classes):
    if num_classes == 3:
        CATEGORIES = CATEGORIES3
    elif num_classes == 5:
        CATEGORIES = CATEGORIES5
    elif num_classes == 12:
        CATEGORIES = CATEGORIES12
    else:
        CATEGORIES = None
    return CATEGORIES


#########################################################################################

def one_hot(data, num_classes):
    enc = OneHotEncoder(handle_unknown='ignore')
    X = np.array([np.arange(0, num_classes)]).reshape(-1, 1)
    enc.fit(X)
    data = data.reshape(-1, 1)
    dx = enc.transform(data).toarray()
    dx = np.array(dx, dtype=np.int32)
    return dx


#########################################################################################


def get_metrics(y_true, y_pred, out, num_classes):
    mcc_score = MatthewsCorrCoef()
    bal_acc_score = BalancedAccuracy()
    avg_prec_score = APScoreBinary(average='weighted')  # APScoreMulti(average='weighted')
    aucroc_score = RocAucBinary(average='weighted')  # RocAuc(average='weighted')

    y_t = y_true.cpu().numpy().flatten()
    y_p = y_pred.cpu().numpy().flatten()

    acc = accuracy_score(y_t, y_p)
    bal_acc = bal_acc_score(y_pred, y_true)
    mcc = mcc_score(y_pred, y_true)

    y_np = one_hot(y_true, num_classes)
    y_torch = torch.from_numpy(y_np)

    apc = avg_prec_score(out, y_torch)
    rocauc = aucroc_score(out, y_torch)

    return acc, bal_acc, mcc, apc, rocauc


########################################################################################
def get_multilabel_metrics(preds, targets, num_classes):

    accuracy_score = Accuracy()
    fbeta_score = FBeta(num_classes=num_classes, beta=2.0)
    roc_auc_score = RocAucMulti(average="weighted")
    average_precision_score = APScoreMulti(average="weighted")

    # Make integer labels
    targets = targets.long()

    thres_acc = accuracy_score(preds, targets).item()

    fbeta = fbeta_score(preds, targets).item()

    roc_auc = roc_auc_score(preds, targets)

    apc = average_precision_score(preds, targets)

    return thres_acc, fbeta, roc_auc, apc


#########################################################################################

# def create_roc_curve(y_true, y_pred, num_classes=12, lw=2, fname="ROC_test.png"):
#     y_true, y_pred = one_hot(y_true, num_classes), one_hot(y_pred, num_classes)
#
#     fpr = dict()
#     tpr = dict()
#     roc_auc = dict()
#
#     # mpl.style.use('seaborn')
#     fig, ax = plt.subplots()
#     fig.figsize = (5 * 2, 4 * 2)
#     for i in range(num_classes):
#         fpr[i], tpr[i], _ = roc_curve(y_true[:, i], y_pred[:, i])
#         roc_auc[i] = auc(fpr[i], tpr[i])
#         plt.plot(fpr[i], tpr[i], lw=lw, label=f'{CATEGORIES[i]} ({roc_auc[i]:.4f})')
#
#     plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
#     plt.xlim([0.0, 1.0])
#     plt.ylim([0.0, 1.05])
#     plt.xlabel('False Positive Rate')
#     plt.ylabel('True Positive Rate')
#     plt.legend(loc="lower right")
#     plt.show()
#     fig.savefig(fname)
#     plt.close()
#     return None


# create_roc_curve(y_true=x, y_pred=y, num_classes=stop, lw=1.5, fname="test.png")

#########################################################################################

def create_confusion_matrix(y_true, y_pred, num_classes, task="test", normalize=True):
    matrix = confusion_matrix(y_true, y_pred, normalize=str(normalize).lower())
    CATEGORIES = get_categories(num_classes)
    classes = CATEGORIES.keys()
    FONT_SIZE = 12

    if num_classes < 6:
        figsize = (5 * 2, 4 * 2)
        FONT_SIZE = 14
    else:
        figsize = (5 * 3, 4 * 3)
        FONT_SIZE = 12

    matplotlib.rcdefaults
    plt.figure(figsize=figsize)  #
    plt.imshow(matrix, cmap=plt.cm.Blues)  # , interpolation='nearest'
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=90, fontsize=FONT_SIZE, fontweight='normal')
    plt.yticks(tick_marks, classes, fontsize=FONT_SIZE, fontweight='normal')
    fmt = '.2f'  # '.3f' if normalize else '.2f'
    thresh = matrix.max() / 2.
    for i, j in itertools.product(range(matrix.shape[0]), range(matrix.shape[1])):
        plt.text(j, i, format(matrix[i, j], fmt),
                 horizontalalignment="center",
                 fontsize=FONT_SIZE + 2,
                 color="white" if matrix[i, j] > thresh else "black")
    plt.tight_layout()
    plt.savefig("images/" + f'{num_classes}_{task}_CM.png', bbox_inches='tight', dpi=100)
    plt.show()
    plt.close()
    return None


