# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 17:58:06 2021

@author: mapo
"""

import os
import os.path as osp
import torch
from torch_geometric.data import Dataset, Data


class MemoryDataset(Dataset):
    def __init__(self, root, transform=None, pre_transform=None):
        super(MemoryDataset, self).__init__(root, transform, pre_transform)

    @property
    def raw_file_names(self):
        return []

    @property
    def processed_file_names(self):
        return [f'data_{i}.pt' for i in range(len([name for name in os.listdir(osp.join(self.root, 'processed'))]) - 2)]



    def process(self):
        i = 0
        for raw_path in self.raw_paths:
            # Read data from `raw_path`.
            data = Data(...)

            if self.pre_filter is not None and not self.pre_filter(data):
                continue

            if self.pre_transform is not None:
                data = self.pre_transform(data)

            torch.save(data, osp.join(self.processed_dir, 'data_{}.pt'.format(i)))
            i += 1

    def len(self):
        return len(self.processed_file_names)

    def get(self, idx):
        data = torch.load(osp.join(self.processed_dir, 'data_{}.pt'.format(idx)))
        return data