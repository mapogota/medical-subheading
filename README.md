# Drug Therapeutic-use Class Prediction

![Distribution](/images/5_joint_plot_val_data_distr.png) ![Drug clusters](/images/5_joint_plot_valid.png)

#### To install run:

	conda create --name <env> --file requirements.txt


#### To test the trained models run:

1.	GraphConvTestSingleLabel jupyter notebook to get test results for single label tasks

2.	GraphConvTestMultiLabel jupyter notebook to get test results for the multi label task



#### To train models on processed data run:

1.		gcn_single_label.py 0 0 for 3 class task on GPU 0

2.		gcn_single_label.py 1 0 for 5 class task on GPU 0

3.		gcn_single_label.py 2 0 for 12 class task on GPU 0
		
4.		gcn_multi_label.py for multi label classification 



![Drug network](/images/pyvis/canvas02.png)