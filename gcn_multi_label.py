import os, sys
import os.path as osp
import numpy as np
import pandas as pd
import torch
import torch_geometric.transforms as T
from torch_geometric.data import DataLoader
from metrics.metrics import get_multilabel_metrics
from utils.dataset import MemoryDataset
from utils.params import reset_weights
from models.graphconv import MultiNetV3

LR = 0.0007
NUM_SPLITS = 5
WORKERS = 12
PATIENCE = 30
NUM_EPOCHS = 750
NUM_CLASSES = 12
GPU = [0, 1, 2, 3, 4, 5, 6, 7][int(sys.argv[1])]

CATEGORIES = {"cns": 0, "antineoplastic": 1, "cardio": 2, "gastrointestinal": 3, "antiinfective": 4,
              "reproductivecontrol": 5, "lipidregulating": 6, "hematologic": 7, "respiratorysystem": 8,
              "antiinflammatory": 9, "urological": 10, "dermatologic": 11}

BATCH_SIZE = 512

net_name = "GraphConv_MultiLabel_v4"

BASE_DIR = os.getcwd()
DATA_DIR = osp.join(BASE_DIR, 'data')
OLD_DATA_DIR = osp.join(DATA_DIR, "multiclass_data")
PROCESSED_DATA_DIR = osp.join(OLD_DATA_DIR, 'processed')
DATA_PATH = osp.join(PROCESSED_DATA_DIR, f'TrainValGraphsData')

OLD_DATA_FILE = osp.join(OLD_DATA_DIR, 'all_chem_df.csv')


def get_class_list(filename):
    data_frame = pd.read_csv(filename)
    classes = []
    cls = [x.split() for x in data_frame['tags']]
    for cl in cls:
        if len(cl) > 1:
            for item in cl:
                classes.append(item)
        else:
            classes.append(cl[0])
    return classes


all_cls = get_class_list(OLD_DATA_FILE)


def get_class_weights(class_list):
    class_weights = {}
    for category in CATEGORIES.keys():
        count = class_list.count(category)
        class_weights[category] = 1 / count

    factor = 1.0 / sum(class_weights.values())
    for g in class_weights.keys():
        class_weights[g] = class_weights[g] * factor
    return class_weights


weights = get_class_weights(all_cls)

weights = torch.from_numpy(np.array(list(weights.values())))


def train(loader):
    model.train()
    total_loss = 0
    for data in loader:
        data = data.to(device)
        optimizer.zero_grad()
        out = model(data)
        labels = data.y.reshape(-1, 12)
        loss = criterion(out, labels).mean()
        loss.backward()
        optimizer.step()
        total_loss += float(loss) * data.num_graphs
    return total_loss / len(loader.dataset)


@torch.no_grad()
def evaluate(loader):
    model.eval()
    total_outs = torch.Tensor()
    total_labels = torch.Tensor()
    for data in loader:
        data = data.to(device)
        out = model(data)
        labels = data.y.reshape(-1, 12)
        total_outs = torch.cat((total_outs, out.cpu()), 0)
        total_labels = torch.cat((total_labels, labels.cpu()), 0)
    return total_labels, total_outs


if __name__ == '__main__':
    device = torch.device(f'cuda:{GPU}' if torch.cuda.is_available() else 'cpu')
    dataset = MemoryDataset(root=DATA_PATH, transform=T.NormalizeFeatures())  #
    model = MultiNetV3(dataset[0].num_features, 1024, NUM_CLASSES).to(device)
    criterion = torch.nn.MultiLabelSoftMarginLoss(weight=weights.to(device), reduction='none')

    all_indices = [i for i in range(0, len(dataset))]

    # Load data already split using stratified CV (sklearn)
    for fold in range(NUM_SPLITS):
        val_file = osp.join(OLD_DATA_DIR, f"multilabel_iter5fold_{fold}.csv")
        data = pd.read_csv(val_file)
        df = pd.DataFrame(data)
        val_indices = [index[0] for index in df.values.tolist() if index[0] <= len(dataset) - 1]
        val_set = set(val_indices)
        full_set = set(all_indices)
        train_set = full_set.difference(val_set)
        train_indices = list(train_set)
        train_data = dataset[train_indices]
        val_data = dataset[val_indices]

        # data loaders
        train_loader = DataLoader(dataset=train_data, batch_size=BATCH_SIZE, shuffle=True,
                                  drop_last=False, num_workers=WORKERS)

        val_loader = DataLoader(dataset=val_data, batch_size=BATCH_SIZE, shuffle=False,
                                drop_last=False, num_workers=WORKERS)

        # Reset model weights, optimizer
        model.apply(reset_weights)
        optimizer = torch.optim.Adam(model.parameters(), lr=LR)


        best_roc_auc = 0.0

        for epoch in range(1, NUM_EPOCHS + 1):
            loss = train(train_loader)
            targets, preds = evaluate(val_loader)
            acc, fbeta, roc_auc, apc = get_multilabel_metrics(preds, targets, NUM_CLASSES)

            print(f'Fold: {fold} | epoch: {epoch:03d} | acc: {acc:.4f} | fbeta: {fbeta:.4f} | roc_auc: {roc_auc:.4f} |'
                  f' apc: {apc:.4f} ')

            if roc_auc > best_roc_auc:
                save_path = f"{net_name}_fold_{fold}.pt"
                try:
                    os.remove(save_path)
                except Exception as e:
                    print(e)
                torch.save(model.state_dict(), save_path)
                print(f"Fold: {fold} | epoch: {epoch:03d} | roc_auc increased {best_roc_auc:.4f} --> {roc_auc:.4f} \n")
                best_roc_auc = roc_auc
