import os, sys
import os.path as osp
import pandas as pd
import torch
import torch.nn.functional as F
import torch_geometric.transforms as T
from torch_geometric.data import DataLoader
from metrics.metrics import get_metrics
from utils.dataset import MemoryDataset
from utils.params import reset_weights
from utils.earlystopping import EarlyStopping
from models.graphconv import Net

LR = 0.0005
NUM_SPLITS = 5
WORKERS = 12
PATIENCE = 30
NUM_EPOCHS = 500
NUM_CLASSES = [3, 5, 12][int(sys.argv[1])]
GPU = [0, 1, 2, 3, 4, 5, 6, 7][int(sys.argv[2])]

if NUM_CLASSES == 3:
    BATCH_SIZE = 256
elif NUM_CLASSES == 5:
    BATCH_SIZE = 256
else:
    BATCH_SIZE = 512

net_name = f"GraphConv_{NUM_CLASSES}"

BASE_DIR = os.getcwd()
DATA_DIR = osp.join(BASE_DIR, 'data')
RAW_DATA_DIR = osp.join(DATA_DIR, "drugclass_rmsalts_rmoverlap")
RAW_DATA_FILE = osp.join(RAW_DATA_DIR, f'{NUM_CLASSES}cls_rmsaltol.csv')
PROCESSED_DATA_DIR = osp.join(DATA_DIR, 'processed')
DATA_PATH = osp.join(PROCESSED_DATA_DIR, f'{NUM_CLASSES}_rmsaltol_data')


def train(loader):
    model.train()
    total_loss = 0
    for data in loader:
        data = data.to(device)
        optimizer.zero_grad()
        out = model(data)
        loss = F.nll_loss(out, data.y)
        loss.backward()
        optimizer.step()
        total_loss += float(loss) * data.num_graphs
    return total_loss / len(loader.dataset)


@torch.no_grad()
def test(loader):
    model.eval()
    total_loss = 0
    total_preds = torch.Tensor()
    total_outs = torch.Tensor()
    total_labels = torch.Tensor()
    for data in loader:
        data = data.to(device)
        out = model(data)
        pred = out.argmax(dim=1)
        loss = F.nll_loss(out, data.y)
        total_loss += float(loss) * data.num_graphs
        total_preds = torch.cat((total_preds, pred.cpu()), 0)
        total_outs = torch.cat((total_outs, out.cpu()), 0)
        total_labels = torch.cat((total_labels, data.y.cpu()), 0)

    total_loss = total_loss / len(loader.dataset)
    return total_labels, total_preds, total_outs, total_loss


if __name__ == '__main__':
    device = torch.device(f'cuda:{GPU}' if torch.cuda.is_available() else 'cpu')
    dataset = MemoryDataset(root=DATA_PATH, transform=T.NormalizeFeatures())  #
    model = Net(dataset[0].num_features, 1024, NUM_CLASSES).to(device)

    all_indices = [i for i in range(0, len(dataset))]

    # Load data already split using stratified CV (sklearn)
    for fold in range(NUM_SPLITS):
        val_file = osp.join(RAW_DATA_DIR, f"{NUM_CLASSES}cls_val_ids{fold}.csv")
        data = pd.read_csv(val_file)
        df = pd.DataFrame(data)
        val_indices = [index[0] for index in df.values.tolist() if index[0] <= len(dataset) - 1]
        val_set = set(val_indices)
        full_set = set(all_indices)
        train_set = full_set.difference(val_set)
        train_indices = list(train_set)
        train_data = dataset[train_indices]
        val_data = dataset[val_indices]

        # data loaders
        train_loader = DataLoader(dataset=train_data, batch_size=BATCH_SIZE, shuffle=True,
                                  drop_last=False, num_workers=WORKERS)

        val_loader = DataLoader(dataset=val_data, batch_size=BATCH_SIZE, shuffle=False,
                                drop_last=False, num_workers=WORKERS)

        # Reset model weights, optimizer
        model.apply(reset_weights)
        optimizer = torch.optim.Adam(model.parameters(), lr=LR)

        best_apc = 0.0

        for epoch in range(1, NUM_EPOCHS + 1):
            loss = train(train_loader)
            y, pred, out, val_loss = test(val_loader)
            
            val_acc, val_bal_acc, val_mcc, val_apc, val_aucroc = get_metrics(y, pred, out, NUM_CLASSES)

            print(
                f'Fold: {fold} | epoch: {epoch:03d} | acc: {val_acc:.4f} | bal_acc: {val_bal_acc:.4f} | mcc: {val_mcc:.4f} |'
                f' aucroc: {val_aucroc:.4f} | apc: {val_apc:.4f}  ')

            if val_apc > best_apc:
                save_path = f"{net_name}_fold_{fold}.pt"
                try:
                    os.remove(save_path)
                except Exception as e:
                    print(e)
                torch.save(model.state_dict(), save_path)
                print(
                    f"Fold: {fold} | epoch: {epoch} | val_apc increased {best_apc:.4f} --> {val_apc:.4f} --saving model ... \n")
                best_apc = val_apc
