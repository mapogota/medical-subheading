import torch
import torch.nn.functional as F
from torch.nn import Linear, BatchNorm1d
from torch_geometric.nn import GraphConv, GraphNorm, global_max_pool

import pytorch_lightning as pl

from metrics.metrics import get_metrics


class Net(torch.nn.Module):
    def __init__(self, in_channels, dim, out_channels):
        super(Net, self).__init__()
        torch.manual_seed(37)

        self.conv1 = GraphConv(in_channels, dim)
        self.conv2 = GraphConv(dim, dim)
        self.conv3 = GraphConv(dim, dim)

        self.gn = GraphNorm(dim)

        self.lin1 = Linear(dim, dim)
        self.lin2 = Linear(dim, out_channels)
        self.bn1 = BatchNorm1d(dim)
        self.bn2 = BatchNorm1d(out_channels)

    def forward(self, data):
        x, edge_index, batch = data.x.float(), data.edge_index, data.batch

        x = self.conv1(x, edge_index)
        x = torch.relu(x)
        x = self.gn(x)

        x = self.conv2(x, edge_index)
        x = torch.relu(x)
        x = self.gn(x)

        x = self.conv3(x, edge_index)
        x = self.gn(x)
        x = torch.relu(x)

        x = global_max_pool(x, batch)

        x = self.lin1(x)
        x = self.bn1(x)
        x = torch.relu(x)
        x = F.dropout(x, p=0.4, training=self.training)
        x = self.lin2(x)
        x = self.bn2(x)
        return F.log_softmax(x, dim=1)
        

class MultiNetV3(torch.nn.Module):
    def __init__(self, in_channels, dim, out_channels):
        super(MultiNetV3, self).__init__()
        torch.manual_seed(37)

        self.conv1 = GraphConv(in_channels, dim)
        self.conv2 = GraphConv(dim, dim)
        self.conv3 = GraphConv(dim, dim)

        self.gn = GraphNorm(dim)

        self.lin1 = Linear(dim, dim)
        self.lin2 = Linear(dim, out_channels)
        self.bn1 = BatchNorm1d(dim)
        self.bn2 = BatchNorm1d(out_channels)

    def forward(self, data):
        x, edge_index, batch = data.x.float(), data.edge_index, data.batch

        x = self.conv1(x, edge_index)
        x = torch.relu(x)
        x = self.gn(x)

        x = self.conv2(x, edge_index)
        x = torch.relu(x)
        x = self.gn(x)

        x = self.conv3(x, edge_index)
        x = self.gn(x)

        x = global_max_pool(x, batch)

        x = self.lin1(x)
        x = self.bn1(x)
        x = torch.relu(x)
        x = F.dropout(x, p=0.25, training=self.training)
        x = self.lin2(x)
        x = self.bn2(x)
        return x
